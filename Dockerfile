FROM golang:1.14
WORKDIR /go/src/gitlab.com/tuliptools/vmproxy
COPY main.go .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /go/src/gitlab.com/tuliptools/vmproxy/main .
CMD ["./main"]
